package com.devcamp.countryregionapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.countryregionapi.Service.RegionService;
import com.devcamp.countryregionapi.models.Region;

public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region GetRegionInfo(@RequestParam(name="code", required = true) String regionCode){
        Region findRegion = regionService.filterRegions(regionCode);
        return findRegion;
    }
    

}

package com.devcamp.countryregionapi.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.models.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN","Ha Noi");
    Region hcm = new Region("HCM","Ho Chi Minh");
    Region danang = new Region("DN","Da Nang");

    Region region1 = new Region("NY","New york");
    Region region2 = new Region("FL","Flodia");
    Region region3 = new Region("TX","Texas");

    Region region4= new Region("OS","Osaka");
    Region region5 = new Region("TK","Tokyo");
    Region region6 = new Region("FU","FUKUOKA");

    public ArrayList<Region> getRegionVN(){
        ArrayList<Region> regionVN = new ArrayList<>();

        regionVN.add(hanoi);
        regionVN.add(hcm);
        regionVN.add(danang);
        return regionVN;
    }

    public ArrayList<Region> getRegionUS(){
        ArrayList<Region> regionUS = new ArrayList<>();

        regionUS.add(region1);
        regionUS.add(region2);
        regionUS.add(region3);
        return regionUS;
    }

    public ArrayList<Region> getRegionJP(){
        ArrayList<Region> regionJP = new ArrayList<>();

        regionJP.add(region4);
        regionJP.add(region5);
        regionJP.add(region6);
        return regionJP;
    }

    //cách 2 lọc data region tạo trực tiếp tại region service

    public Region filterRegions(String regionCode){
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(hanoi);
        regions.add(hcm);
        regions.add(danang);
        regions.add(region1);
        regions.add(region2);
        regions.add(region3);
        regions.add(region4);
        regions.add(region5);
        regions.add(region6);
        
        Region findRegion = new Region();
        for (Region regionElement : regions){
            if(regionElement.getRegionCode().equals(regionCode)){
                findRegion = regionElement;
            }
        }
        return findRegion;
    }

}

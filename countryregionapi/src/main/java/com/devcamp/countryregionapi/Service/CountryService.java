package com.devcamp.countryregionapi.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.models.Country;

@Service
public class CountryService {
    @Autowired

    private RegionService regionService;
    Country vietnam = new Country("VN", "Viet Nam", null);
    Country usa = new Country("USA", "USA", null);
    Country japan = new Country("JP", "Japan", null);

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = new ArrayList<>();

        vietnam.setRegions(regionService.getRegionVN());
        usa.setRegions(regionService.getRegionUS());
        japan.setRegions(regionService.getRegionJP());

        allCountry.add(vietnam);
        allCountry.add(usa);
        allCountry.add(japan);

        return allCountry;

    }

    public Country getIndexCountry(int index){
        Country country = null;
        ArrayList<Country> allCountry = getAllCountries();
        if (index >= 0 && index <= allCountry.size())
        country = allCountry.get(index);
        return country;

        }

}

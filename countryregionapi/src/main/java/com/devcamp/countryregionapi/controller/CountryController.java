package com.devcamp.countryregionapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.Service.CountryService;
import com.devcamp.countryregionapi.models.Country;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")

    public ArrayList<Country> getAllCountries(){
        ArrayList<Country> allCountry = countryService.getAllCountries();
        return allCountry;
    }
    
    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam(name="code", required = true)String countryCode){
        ArrayList<Country> allCountry = countryService.getAllCountries();
         
        Country findCountry = new Country();
        for (Country countryElement: allCountry){
            if(countryElement.getCountryCode().equals(countryCode)){
                findCountry = countryElement;
            }
        }
            return findCountry;
            
        }


    @GetMapping("/countries/{index}")
    public Country getIndexCountry(@PathVariable int index){
        return countryService.getIndexCountry(index);
    }
}
